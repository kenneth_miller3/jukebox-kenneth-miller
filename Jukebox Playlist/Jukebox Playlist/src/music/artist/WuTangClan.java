package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class WuTangClan {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public WuTangClan() {
    }
    
    public ArrayList<Song> getWuTangClanSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Bring Da Ruckus", "WuTangClan");         				//Create a song
         Song track2 = new Song("Da Mystery of Chessboxin", "WuTangClan");        //Create another song
         Song track3 = new Song("Triumph", "WuTangClan");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for WuTangClan in the form of an ArrayList
    }
}