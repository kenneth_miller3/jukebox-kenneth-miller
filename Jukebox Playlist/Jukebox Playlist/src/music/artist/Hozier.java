package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Hozier {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Hozier() {
    }
    
    public ArrayList<Song> getHozierSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Take Me to Church", "Hozier");             	//Create a song
         Song track2 = new Song("Work Song", "Hozier");         				//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Hozier
         this.albumTracks.add(track2);                                          //Add the second song to song list for Hozier 
         return albumTracks;                                                    //Return the songs for Hozier in the form of an ArrayList
    }
}
