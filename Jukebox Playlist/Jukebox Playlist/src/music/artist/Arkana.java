package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Arkana {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Arkana() {
    }
    
    public ArrayList<Song> getArkanaSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Ein Sof", "Arkana");         				//Create a song
         Song track2 = new Song("Genesis", "Arkana");        //Create another song
         Song track3 = new Song("Kodama", "Arkana");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for Arkana in the form of an ArrayList
    }
}