package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class JimmyEatWorld {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public JimmyEatWorld() {
    }
    
    public ArrayList<Song> getJimmyEatWorldSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("The Middle", "Jimmy Eat World");            	//Create a song
         Song track2 = new Song("Pain", "Jimmy Eat World");      	  			//Create another song
         Song track3 = new Song("Sweetness", "Jimmy Eat World");      	  		//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Jimmy Eat World
         this.albumTracks.add(track2);                                          //Add the second song to song list for Jimmy Eat World
         this.albumTracks.add(track3);                                          //Add the second song to song list for Jimmy Eat World
         return albumTracks;                                                    //Return the songs for Jimmy Eat World in the form of an ArrayList
    }
}
