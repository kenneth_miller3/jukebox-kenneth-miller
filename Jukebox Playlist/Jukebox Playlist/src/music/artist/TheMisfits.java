package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class TheMisfits {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public TheMisfits() {
    }
    
    public ArrayList<Song> getTheMisfitsSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Where Eagles Dare", "The Misfits");             //Create a song
         Song track2 = new Song("Die, Die My Darling", "The Misfits");         //Create another song         
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Misfits
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Misfits         
         return albumTracks;                                                    //Return the songs for the Misfits in the form of an ArrayList
    }
}
