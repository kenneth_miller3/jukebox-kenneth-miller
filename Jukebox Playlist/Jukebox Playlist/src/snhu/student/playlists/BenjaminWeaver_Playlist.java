package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class BenjaminWeaver_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> ArkanaSongs = new ArrayList<Song>();
    Arkana Arkana = new Arkana();
	
    ArkanaSongs = Arkana.getArkanaSongs();
	
	playlist.add(ArkanaSongs.get(0));
	playlist.add(ArkanaSongs.get(1));
	playlist.add(ArkanaSongs.get(2));
	
	
    HEXSYSTEM HEXSYSTEM = new HEXSYSTEM();
	ArrayList<Song> HEXSYSTEMsongs = new ArrayList<Song>();
    HEXSYSTEMsongs = HEXSYSTEM.getHEXSYSTEMSongs();
	
	playlist.add(HEXSYSTEMsongs.get(0));
	playlist.add(HEXSYSTEMsongs.get(1));
	playlist.add(HEXSYSTEMsongs.get(2));
	
    return playlist;
	}
}
