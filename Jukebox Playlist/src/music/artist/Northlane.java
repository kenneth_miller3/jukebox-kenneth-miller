package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Northlane {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Northlane() {
    }
    
    public ArrayList<Song> getNorthlaneSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Quantum Flux", "Northlane");                   //Create a song
         Song track2 = new Song("4D", "Northlane");                             //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Northlane
         this.albumTracks.add(track2);                                          //Add the second song to song list for Northlane
         return albumTracks;                                                    //Return the songs for Northlane in the form of an ArrayList
    }
}
