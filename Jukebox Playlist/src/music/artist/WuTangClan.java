package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class WuTangClan {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public WuTangClan() {
    }
    
    public ArrayList<Song> getWuTangSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Triumph", "Wu-Tang Clan");             //Create a song
         Song track2 = new Song("Bring Da Ruckus", "Wu-Tang Clan");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the WuTangClan
         this.albumTracks.add(track2);                                          //Add the second song to song list for the WuTangClan 
         return albumTracks;                                                    //Return the songs for the WuTangClan in the form of an ArrayList
    }
}
