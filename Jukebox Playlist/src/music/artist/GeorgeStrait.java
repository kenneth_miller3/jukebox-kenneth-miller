package music.artist;
import snhu.jukebox.playlist.Song;
import java.util.ArrayList;


public class GeorgeStrait {
	
		
		ArrayList<Song> albumTracks;
	    String albumTitle;
	    
	    public GeorgeStrait() {
	    }
	    
	    public ArrayList<Song> getBeatlesSongs() {
	    	
	    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
	    	 Song track1 = new Song("Give it all we got", "George Strait");             //Create a song
	         Song track2 = new Song("Amarillo By Morning", "George Strait");         //Create another song
	         this.albumTracks.add(track1);                                          //Add the first song to song list for George Strait
	         this.albumTracks.add(track2);                                          //Add the second song to song list for George Strait 
	         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
	    }
	}


