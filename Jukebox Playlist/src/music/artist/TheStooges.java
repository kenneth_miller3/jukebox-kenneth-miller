package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class TheStooges {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public TheStooges() {
    }
    
    public ArrayList<Song> getStoogesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Down on the Street", "The Stooges");             //Create a song
         Song track2 = new Song("I Wanna Be Your Dog", "The Stooges");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Stooges
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Stooges 
         return albumTracks;                                                    //Return the songs for the Stooges in the form of an ArrayList
    }
}
