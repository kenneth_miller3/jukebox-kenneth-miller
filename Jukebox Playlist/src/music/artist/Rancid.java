package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Rancid {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Rancid() {
    }
    
    public ArrayList<Song> getRancidSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Old Friend", "Rancid");                        //Create a song
         Song track2 = new Song("I Wanna Riot", "Rancid");			            //Create another song
         Song track3 = new Song("The Wars End", "Rancid");						//Create a third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Rancid
         this.albumTracks.add(track2);                                          //Add the second song to song list for Rancid
         this.albumTracks.add(track3);											//Add the third song to song list for Rancid
         return albumTracks;                                                    //Return the songs for Rancid in the form of an ArrayList
    }
}
