package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class AmosLee {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public AmosLee() {
    }
    
    public ArrayList<Song> getAmosLeeSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                       //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Vaporize", "AmosLee");             //Create a song
         Song track2 = new Song("Wait Up For Me", "AmosLee");             //Create another song
    
         this.albumTracks.add(track1);                             //Add the first song to song list for AmosLee
         this.albumTracks.add(track2);                             //Add the second song to song list for AmosLee

         return albumTracks;                                       //Return the songs for AmosLee in the form of an ArrayList
    }
}