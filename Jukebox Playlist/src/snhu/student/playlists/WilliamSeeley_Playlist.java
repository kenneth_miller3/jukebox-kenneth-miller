package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class WilliamSeeley_Playlist  {
	    
	public LinkedList<PlayableSong> StudentPlaylist(){
		
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> RancidTracks = new ArrayList<Song>();
		Rancid rancidBand = new Rancid();
		
		RancidTracks = rancidBand.getRancidSongs();
		
		playlist.add(RancidTracks.get(0));
		playlist.add(RancidTracks.get(1));
		playlist.add(RancidTracks.get(2));
		
		
		GogolBordello gogolBordelloBand = new GogolBordello();
		ArrayList<Song> gogolBordelloTracks = new ArrayList<Song>();
		gogolBordelloTracks = gogolBordelloBand.getGogolBordelloSongs();
		
		playlist.add(gogolBordelloTracks.get(0));
		playlist.add(gogolBordelloTracks.get(1));
		
		TheStooges theStoogesBand = new TheStooges();
		ArrayList<Song> theStoogesTracks = new ArrayList<Song>();
		theStoogesTracks = theStoogesBand.getStoogesSongs();
		
		playlist.add(theStoogesTracks.get(0));
		playlist.add(theStoogesTracks.get(1));
		
		
		WuTangClan wuTangClanBand = new WuTangClan();
		ArrayList<Song> wuTangClanTracks = new ArrayList<Song>();
		wuTangClanTracks = wuTangClanBand.getWuTangSongs();
		
		playlist.add(wuTangClanTracks.get(0));
		playlist.add(wuTangClanTracks.get(1));
		
		return playlist;
	}
}
