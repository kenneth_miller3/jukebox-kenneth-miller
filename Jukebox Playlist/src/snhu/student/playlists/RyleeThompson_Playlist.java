package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class RyleeThompson_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
    // Create playlist
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
	// Add Issues songs to playlist
	ArrayList<Song> issuesTracks = new ArrayList<Song>(); // Creates ArrayList to hold songs
    Issues issuesBand = new Issues(); 				      // Creates instance of band class
	
    issuesTracks = issuesBand.getIssuesSongs();			  // Gets songs and places them in ArrayList
	
	playlist.add(issuesTracks.get(0));				      // Retrieves songs from ArrayList and adds them to playlist
	playlist.add(issuesTracks.get(1));
	playlist.add(issuesTracks.get(2));
	
	// Add Northlane songs to playlist
	ArrayList<Song> northlaneTracks = new ArrayList<Song>();
    Northlane northlaneBand = new Northlane();
	
    northlaneTracks = northlaneBand.getNorthlaneSongs();
	
	playlist.add(northlaneTracks.get(0));
	playlist.add(northlaneTracks.get(1));

	// Add Jimmy Eat World songs to playlist
	ArrayList<Song> jimmyEatWorldTracks = new ArrayList<Song>();
    JimmyEatWorld jimmyEatWorldBand = new JimmyEatWorld();
	
    jimmyEatWorldTracks = jimmyEatWorldBand.getJimmyEatWorldSongs();
	
	playlist.add(jimmyEatWorldTracks.get(0));
	playlist.add(jimmyEatWorldTracks.get(1));
	
    return playlist;
	}
}